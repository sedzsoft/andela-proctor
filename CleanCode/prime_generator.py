# This class Generates prime numbers up to a user specified
# maximum. The algorithm used is the Sieve of Eratosthenes.
# Given an array of integers starting at 2:
# Find the first uncrossed integer, and cross out all its
# multiples. Repeat until there are no more multiples
# in the array.

# Every multiple in the array has a prime factor that
# is less than or equal to the root of the array size,
# so we don't have to cross out multiples of numbers
# larger than that root.


class PrimeGenerator:

    def __init__(self):
        self.crossed_out = []
        self.primes = []

    def generate_primes(self, max_value):
        if max_value < 2:
            return []
        else:
            self._uncross_integers_up_to(max_value)
            self._cross_out_multiples()
            self._put_uncrossed_integers_into_primes()
            return self.primes

    def _uncross_integers_up_to(self, max_value):
        self.crossed_out = [False for _ in range(max_value + 1)]

    def _cross_out_multiples(self):
        for i in range(2, len(self.crossed_out)/2):
            if self._not_crossed(i):
                self._cross_out_multiples_of(i)

    def _not_crossed(self, number):
        return self.crossed_out[number] is False

    def _cross_out_multiples_of(self, number):
        initial = 2 * number
        for multiple in range(initial, len(self.crossed_out), number):
            self.crossed_out[multiple] = True

    def _put_uncrossed_integers_into_primes(self):
        for number in range(2, len(self.crossed_out)):
            if self._not_crossed(number):
                self.primes.append(number)


if __name__ == '__main__':
    try:
        prime_generator = PrimeGenerator()
        max_value = int(raw_input('Enter maximum value: '))
        print prime_generator.generate_primes(max_value)
    except ValueError:
        print 'Error: Non-integer value entered.'


