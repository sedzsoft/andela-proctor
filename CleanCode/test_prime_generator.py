import unittest
from prime_generator import PrimeGenerator


class PrimeGeneratorTests(unittest.TestCase):
    def setUp(self):
        self.prime_generator = PrimeGenerator()

    def test_generate_primes_receives_max_value_argument(self):
        max_value = 5
        self.prime_generator.generate_primes(max_value)

    def test_generate_primes_return_empty_primes_list_if_max_value_is_less_than_2(self):
        max_value = -10
        primes = self.prime_generator.generate_primes(max_value)
        self.assertEqual(primes, [])

    def test_generate_primes_returns_primes_list_less_or_equal_to_the_max_value(self):
        max_value = 10
        primes = self.prime_generator.generate_primes(max_value)
        self.assertEqual(primes, [2, 3, 5, 7])


