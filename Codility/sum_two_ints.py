import unittest


def sum_two_ints(numbers):
    if isinstance(numbers, list):
        return sum([int(a) for a in numbers if len(str(abs(a))) == 2])


print sum_two_ints([-2, 4, 5, 10, 1000, 24])
print sum_two_ints([-15, 45, 1, 10, 1, 13])
print sum_two_ints('Woof')


class SumTwoIntsTests(unittest.TestCase):
    """Test sum_two_ints function"""

    def setUp(self):
        self.numbers = [2, 4, 14, 15]

    def tearDown(self):
        pass

    def test_function_runs(self):
        sum_two_ints(self.numbers)

    def test_function_return_correct_sum_of_tow_digit_numbers(self):
        self.assertEqual(sum_two_ints(self.numbers), 29)

    def test_function_return_none_if_parameter_is_non_list(self):
        self.assertEqual(sum_two_ints('Woof'), None)

    # def test_raises_type_error_when_non_list_value_is_passed(self):
    #     with self.assertRaises(TypeError):
    #         sum_two_ints('Woof')


if __name__ == '__main__':
    unittest.main()
