def fizz_buzz_woof(number):
    if not isinstance(number, int) or not isinstance(number, float):
        raise TypeError('Must pass an integer or float value')

    output = ''
    if number % 3 == 0:
        output += 'Fizz'
    if number % 5 == 0:
        output += 'Buzz'
    if number % 7 == 0:
        output += 'Woof'

    if output:
        print output
    else:
        print number


for number in range(1, 110):
    fizz_buzz_woof(number)

fizz_buzz_woof('Woof')


