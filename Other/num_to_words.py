def nums_to_words(number):
    digit_to_word = {1: "One", 2: "Two", 3: "Three", 4: "Four", 5: "Five",
                     6: "Six", 7: "Seven", 8: "Eight", 9: "Nine", 0: "Zero"}
    split_numbers = [int(x) for x in "".join(str(d) for d in str(number))]

    words = ""
    for digit in split_numbers:
        words += digit_to_word[digit] + " "

    return words

print nums_to_words(1)
print nums_to_words(12)
print nums_to_words(123534)