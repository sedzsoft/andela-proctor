def sum_of_numbers(number):
    total = 0
    while number > 0:
        quotient, remainder = divmod(number, 10)
        total += remainder
        number = quotient
    return total


def sum_of_numbers_small(number):
    return sum([int(a) for a in str(number)])

print sum_of_numbers(4444)
print sum_of_numbers_small(0)
