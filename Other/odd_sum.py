def odd_sum(numbers):
    odd_numbers = []
    for digit in numbers:
        if digit % 2 != 0:
            odd_numbers.append(digit)

    total = 0
    for odd in odd_numbers:
        total += odd

    return total


def odd_sum_small(numbers):
    if isinstance(numbers, list):
        return sum([x for x in numbers if x % 2 != 0])


print odd_sum([1, 2, 3])
print odd_sum([1, 5, 3])

print odd_sum_small([1, 2, 3, 4, 5, 6, 7])
