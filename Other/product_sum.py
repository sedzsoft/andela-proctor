def product_and_sum(numbers):
    if not isinstance(numbers, list):
        return 'Parameter must be list of integers'

    single_numbers = [int(a) for a in "".join([str(y) for y in numbers])]
    unique_numbers = set(single_numbers)

    product_of_numbers = 1
    for num in unique_numbers:
        product_of_numbers *= num

    return sum(unique_numbers), product_of_numbers


print product_and_sum([1, 2, 5677, 234])
print product_and_sum(5677)

