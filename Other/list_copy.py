import copy


def change_value(old_list, pos):
    new_list = [list(x) for x in old_list]
    new_list[pos[0]][pos[1]] = "X"

    return old_list, new_list


def deep_solution(old_list, pos):
    new_list = copy.deepcopy(old_list)
    new_list[pos[0]][pos[1]] = "X"

    return old_list, new_list


print change_value([[1, 2, 3], [4, 5, 6]], [0, 2])
print deep_solution([[7, 8, 9], [10, 11, 12]], [0, 2])

