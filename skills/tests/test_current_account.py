from unittest import TestCase

from skills.src.account import CurrentAccount


class TestCurrentAccount(TestCase):
    def setUp(self):
        self.current_account = CurrentAccount()

    def test_current_account_minimum_balance_is_1000(self):
        self.assertEqual(self.current_account.minimum_balance, 1000)
