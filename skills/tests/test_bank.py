from unittest import TestCase

from skills.errors.account_errors import MinimumBalanceExceeded
from skills.src.bank import Bank
from skills.src.customer import Customer


class TestBank(TestCase):
    def setUp(self):
        self.bank = Bank()
        self.customer = Customer("Daniel")

    def test_open_current_account_creates_current_account(self):
        message, account_number = self.bank.open_current_account(self.customer)

        self.assertListEqual([len(self.bank.customers), len(self.bank.current_accounts)], [1, 1])
        self.assertEqual(message, "Current account for Daniel created.")

    def test_open_savings_account_creates_savings_account(self):
        message, account_number = self.bank.open_savings_account(self.customer)

        self.assertListEqual([len(self.bank.customers), len(self.bank.savings_accounts)], [1, 1])
        self.assertEqual(message, "Savings account for Daniel created.")

    def test_customer_can_deposit_money_on_current_account(self):
        _, account_number = self.bank.open_current_account(self.customer)
        self.bank.deposit(account_number, 1000)

        self.assertEqual(self.bank.current_accounts.get(account_number).balance, 2000)

    def test_customer_can_deposit_money_on_savings_account(self):
        _, account_number = self.bank.open_savings_account(self.customer)
        self.bank.deposit(account_number, 500)

        self.assertEqual(self.bank.savings_accounts.get(account_number).balance, 500)

    def test_customer_can_withdraw_from_current_account(self):
        _, account_number = self.bank.open_current_account(self.customer)
        self.bank.deposit(account_number, 1000)
        self.bank.withdraw(account_number, 500)

        self.assertEqual(self.bank.current_accounts.get(account_number).balance, 1500)

    def test_withdraw_fails_if_balance_will_be_less_than_the_minimum_balance(self):
        _, account_number = self.bank.open_current_account(self.customer)
        self.bank.deposit(account_number, 1000)

        self.assertRaises(MinimumBalanceExceeded, self.bank.withdraw, account_number, 1500)

    def test_customer_can_withdraw_from_savings_account(self):
        _, account_number = self.bank.open_savings_account(self.customer)
        self.bank.deposit(account_number, 1000)
        self.bank.withdraw(account_number, 500)

        self.assertEqual(self.bank.savings_accounts.get(account_number).balance, 500)

    def test_customer_can_withdraw_an_overdraft(self):
        _, account_number = self.bank.open_savings_account(self.customer)
        self.bank.withdraw(account_number, 3000)

        self.assertEqual(self.bank.savings_accounts.get(account_number).balance, -3000)

    def test_overdraft_withdraw_fails_if_customer_draws_over_minimum_balance(self):
        _, account_number = self.bank.open_savings_account(self.customer)

        self.assertRaises(MinimumBalanceExceeded, self.bank.withdraw, account_number, 4000)
