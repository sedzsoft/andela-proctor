from unittest import TestCase

from skills.src.account import SavingsAccount


class TestSavingsAccount(TestCase):
    def setUp(self):
        self.savings_account = SavingsAccount()

    def test_savings_account_minimum_balance_is_negative_3000(self):
        self.assertEqual(self.savings_account.minimum_balance, -3000)
