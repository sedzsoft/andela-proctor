from unittest import TestCase

from skills.src.customer import Customer


class TestCustomer(TestCase):
    def setUp(self):
        self.customer_name = 'Alice Wambui'
        self.customer = Customer(self.customer_name)

    def test_customer_responds_to_properties(self):
        self.assertEqual(self.customer.name, self.customer_name)
