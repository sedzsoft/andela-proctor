import random

from skills.errors.account_errors import MinimumBalanceExceeded


class Account(object):
    def __init__(self, initial_balance, minimum_balance):
        if type(self) == Account:
            raise NotImplementedError("Can not instantiate Account directly.")
        self.balance = initial_balance
        self.minimum_balance = minimum_balance

    @staticmethod
    def generate_account_number():
        return random.randrange(1000, 9999)

    def deposit(self, amount):
        self.balance += amount

    def withdraw(self, amount):
        if (self.balance - amount) < self.minimum_balance:
            raise MinimumBalanceExceeded("Withdrawal amount exceeded limit.")
        self.balance -= amount


class CurrentAccount(Account):
    def __init__(self):
        self.minimum_balance = 1000
        super(self.__class__, self).__init__(self.minimum_balance, self.minimum_balance)
        self.account_number = self.__class__.generate_account_number()


class SavingsAccount(Account):
    def __init__(self):
        self.minimum_balance = -3000
        self.initial_balance = 0
        super(self.__class__, self).__init__(self.initial_balance, self.minimum_balance)
        self.account_number = self.__class__.generate_account_number()
