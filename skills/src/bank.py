from skills.src.account import CurrentAccount, SavingsAccount


class Bank(object):
    def __init__(self):
        self.savings_accounts = {}
        self.current_accounts = {}
        self.customers = []

    def open_current_account(self, customer):
        current_account = CurrentAccount()
        account_number = current_account.account_number
        customer.current_account_number = account_number
        self.customers.append(customer)
        self.current_accounts[account_number] = current_account
        return 'Current account for {0} created.'.format(customer.name), account_number

    def open_savings_account(self, customer):
        savings_account = SavingsAccount()
        account_number = savings_account.account_number
        customer.savings_account_number = account_number
        self.customers.append(customer)
        self.savings_accounts[account_number] = savings_account
        return 'Savings account for {0} created.'.format(customer.name), account_number

    def deposit(self, account_number, amount):
        if account_number in self.current_accounts:
            current_account = self.current_accounts[account_number]
            current_account.deposit(amount)

        if account_number in self.savings_accounts:
            savings_account = self.savings_accounts[account_number]
            savings_account.deposit(amount)

    def withdraw(self, account_number, amount):
        if account_number in self.current_accounts:
            current_account = self.current_accounts[account_number]
            current_account.withdraw(amount)

        if account_number in self.savings_accounts:
            savings_account = self.savings_accounts[account_number]
            savings_account.withdraw(amount)
