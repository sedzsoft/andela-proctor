def manipulate_data(numbers):
    if not isinstance(numbers, list):
        return 'Only lists allowed'

    positive_count = 0
    negative_sum = 0
    for number in numbers:
        if isinstance(number, int):
            if number >= 0:
                positive_count += 1
            else:
                negative_sum += number
    return [positive_count, negative_sum]


print manipulate_data(1)
print manipulate_data([1, 2, 3, 4])
print manipulate_data([1, -9, 2, 3, 4, -5])
print manipulate_data(['str', [], 2, 3, -4, -5])
