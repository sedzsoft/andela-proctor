from abc import ABCMeta, abstractmethod


class BankAccount(object):
    minimum_balance = 0

    __metaclass__ = ABCMeta

    @abstractmethod
    def withdraw(self, amount):
        pass

    @abstractmethod
    def deposit(self, amount):
        pass

    @staticmethod
    def is_amount_valid(amount):
        if amount is None or amount < 0 or not (isinstance(amount, int) or isinstance(amount, float)):
            return False
        return True


class SavingsAccount(BankAccount):
    minimum_balance = 500

    def __init__(self):
        self.balance = self.__class__.minimum_balance

    def deposit(self, amount=None):
        if not self.__class__.is_amount_valid(amount):
            return 'Invalid deposit amount'

        self.balance += amount
        return self.balance

    def withdraw(self, amount=None):
        if not self.__class__.is_amount_valid(amount):
            return 'Invalid withdraw amount'

        if amount > self.balance:
            return 'Cannot withdraw beyond the current account balance'

        if SavingsAccount.minimum_balance >= (self.balance - amount):
            return 'Cannot withdraw beyond the minimum account balance'

        self.balance -= amount
        return self.balance


class CurrentAccount(BankAccount):
    minimum_balance = 0

    def __init__(self):
        self.balance = self.__class__.minimum_balance

    def deposit(self, amount=None):
        if not self.__class__.is_amount_valid(amount):
            return 'Invalid deposit amount'

        self.balance += amount
        return self.balance

    def withdraw(self, amount=None):
        if not self.__class__.is_amount_valid(amount):
            return 'Invalid withdraw amount'

        if amount > self.balance:
            return 'Cannot withdraw beyond the current account balance'

        self.balance -= amount
        return self.balance


# ba = BankAccount()

print ''
print '============================================'
print ''

sa = SavingsAccount()
print str(sa.balance) + " = 500"

sa = SavingsAccount()
sa.deposit(1500)
print str(sa.balance) + " = 2000"

sa = SavingsAccount()
print sa.withdraw(1500)

sa = SavingsAccount()
sa.deposit(2300)
print sa.withdraw(2500)
print str(sa.balance) + " = 2257"
print sa.deposit({})
print sa.withdraw({})
print sa.deposit()
print sa.withdraw()
print sa.deposit('str')
print sa.withdraw('str')
print sa.deposit(-300.5)
print sa.withdraw(-450.5)

# print ''
# print '============================================'
# print ''
#
# ca = CurrentAccount()
# ca.deposit(1500)
# print ca.balance
#
# ca = CurrentAccount()
# print ca.withdraw(1500)
# print ca.balance
#
# ca = CurrentAccount()
# ca.deposit(23001)
# ca.withdraw(437)
# print str(ca.balance) + " = 22564"
# print ca.deposit(0)
# print ca.withdraw(0)
# print ca.withdraw({})
# print ca.deposit({})
# print ca.withdraw('str')
# print ca.deposit('str')
# print ca.withdraw(-1000)
# print ca.deposit(-700)
