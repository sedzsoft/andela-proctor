def replicate_iter(times, data):
    if not isinstance(times, int) or not (isinstance(data, int) or isinstance(data, str)):
        raise ValueError('Times must be a positive integer. Data must be a number or text.')

    replicated_data = []
    if times <= 0:
        return replicated_data

    while times > 0:
        replicated_data.append(data)
        times -= 1
    return replicated_data


def replicate_recur(times, data, replicated_data=None):
    if replicated_data is None:
        replicated_data = []

    if not isinstance(times, int) or not (isinstance(data, int) or isinstance(data, str)):
        raise ValueError('Times must be a positive integer. Data must be a number or text.')

    if times <= 0:
        return replicated_data

    replicated_data.append(data)
    return replicate_recur(times - 1, data, replicated_data)


print replicate_iter(5, 'becks')
print replicate_recur(-1, 5)

# print replicate_iter(2,[])
# print replicate_iter([],5)

# print replicate_recur([],3)
# print replicate_recur(3, [])
