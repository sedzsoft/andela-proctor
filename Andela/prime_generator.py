def prime_generator(number):
    """
    Generate prime numbers up to the upper bound passed as an argument
    :param
        number: Upper bound of primes
    :return:
        List of prime numbers
    """

    if not isinstance(number, int):
        return 'Invalid input'

    if number < 2:
        return 'Number must be >= 2'

    primes = []
    for num in range(2, number + 1):
        is_prime = True
        divisor = 2
        while divisor < num:
            if num % divisor == 0:
                is_prime = False
            divisor += 1
        if is_prime:
            primes.append(num)
    return primes
