def product_of_digits(numbers):
    result = []
    numbers.sort()
    for x in range(len(numbers)/2):
        result.append(numbers[-x-1] * numbers[x])
    return result

print product_of_digits([5, 9, 2, 4, 1, 3])
print product_of_digits([5, 9, 2, 4, 1])

