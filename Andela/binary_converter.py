def binary_converter(number):
    if number > 255 or number < 0 or not isinstance(number, int):
        return 'Invalid input'

    if number == 0:
        return '0'

    binary_string = ''
    while number > 0:
        quotient, binary = divmod(number, 2)
        binary_string = str(binary) + binary_string
        number = quotient
    return binary_string


print binary_converter(1)
print binary_converter(-5)
print binary_converter(3)
print binary_converter(237)
print binary_converter(0)
print binary_converter(3533465456)
print binary_converter([])
