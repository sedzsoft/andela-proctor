def calculate_tax(citizens):
    if not isinstance(citizens, dict):
        raise ValueError('No parameters or wrong parameters provided!')

    citizens_taxes = {}
    for citizen_name, citizen_pay in citizens.items():
        total_tax = 0

        if isinstance(citizen_pay, int) or isinstance(citizen_pay, float):
            if citizen_pay >= 0:  # Tax rate 0% for pay < 1000
                total_tax = 0

            if citizen_pay >= 1001:  # Tax rate 10% for pay 1001 - 10000
                taxable_amount, lower_limit, upper_limit = 9000, 1000, 10000
                total_tax += tax(citizen_pay, taxable_amount, upper_limit, lower_limit, 0.1)

            if citizen_pay >= 10001:  # Tax rate 15% for pay 10001 - 20200
                taxable_amount, lower_limit, upper_limit = 10200, 10000, 20200
                total_tax += tax(citizen_pay, taxable_amount, upper_limit, lower_limit, 0.15)

            if citizen_pay >= 20201:  # Tax rate 20% for pay 20201 - 30750
                taxable_amount, lower_limit, upper_limit = 10550, 20200, 30750
                total_tax += tax(citizen_pay, taxable_amount, upper_limit, lower_limit, 0.2)

            if citizen_pay >= 30751:  # Tax rate 25% for pay 30750 - 50000
                taxable_amount, lower_limit, upper_limit = 19250, 30750, 50000
                total_tax += tax(citizen_pay, taxable_amount, upper_limit, lower_limit, 0.25)

            if citizen_pay > 50000:  # tax rate 30% for pay > 50000
                lower_limit = 50000
                total_tax += (citizen_pay - lower_limit) * 0.3
        else:
            raise ValueError('Invalid amount provided')

        citizens_taxes[citizen_name] = total_tax

    return citizens_taxes


def tax(citizen_pay, taxable_amount, upper_limit, lower_limit, tax_percentage):
    if citizen_pay > upper_limit:
        return taxable_amount * tax_percentage
    else:
        return (citizen_pay - lower_limit) * tax_percentage


citizens = {"James": 2490.0, "Kiura": 200, "Kinuthia": 15352.5}
print calculate_tax(citizens)
